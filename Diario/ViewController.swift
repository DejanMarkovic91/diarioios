//
//  ViewController.swift
//  Diario
//
//  Created by Dejan Markovic on 11/25/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

import UIKit
import Firebase
import Crashlytics

class ViewController: UIViewController, GIDSignInUIDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //test
        emailField.text = "bodemarkovic@gmail.com";
        passwordField.text = "jebemkolud";
        //login(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUp(_ sender: Any) {
        FIRAuth.auth()?.createUser(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
            if let err = error {
                print(err.localizedDescription)
                Answers.logLogin(withMethod: "regular", success: false, customAttributes: nil)
                self.showAlertDialog(title: "Error", message: err.localizedDescription)
            } else {
                Crashlytics.sharedInstance().setUserEmail(user?.email)
                Crashlytics.sharedInstance().setUserIdentifier(user?.uid)
                Crashlytics.sharedInstance().setUserName(user?.email)
                Answers.logLogin(withMethod: "regular", success: true, customAttributes: nil)
                self.performSegue(withIdentifier: "authenticationSuccessSegue", sender: self)
            }
        }
    }
    
    @IBAction func login(_ sender: Any) {
        if((emailField.text?.isEmpty)! || (passwordField.text?.isEmpty)!){
            showAlertDialog(title: "Error", message: "Email and password are required!");
            return;
        }
        FIRAuth.auth()?.signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
            if let err = error {
                print(err.localizedDescription)
                Answers.logLogin(withMethod: "regular", success: false, customAttributes: nil)
                self.showAlertDialog(title: "Error", message: err.localizedDescription)
            } else {
                Crashlytics.sharedInstance().setUserEmail(user?.email)
                Crashlytics.sharedInstance().setUserIdentifier(user?.uid)
                Crashlytics.sharedInstance().setUserName(user?.email)
                Answers.logLogin(withMethod: "regular", success: true, customAttributes: nil)
                self.performSegue(withIdentifier: "authenticationSuccessSegue", sender: self)
            }
        }
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if (error != nil) {
            showAlertDialog(title: "Error", message: error.localizedDescription)
        } else {
            Crashlytics.sharedInstance().setUserEmail(user?.profile.email)
            Crashlytics.sharedInstance().setUserIdentifier(user?.userID)
            Crashlytics.sharedInstance().setUserName(user?.profile.name)
            Answers.logLogin(withMethod: "google", success: true, customAttributes: nil)
            self.performSegue(withIdentifier: "authenticationSuccessSegue", sender: self)
        }
    }
    
    func showAlertDialog(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

