//
//  NoteCellControllerTableViewCell.swift
//  Diario
//
//  Created by Dejan Markovic on 11/26/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

import UIKit

class NoteCellControllerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var headerView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
