//
//  NotesViewController.swift
//  Diario
//
//  Created by Dejan Markovic on 11/26/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

import UIKit
import Firebase

var editingNote: Note = Note();
var notes: [Note] = [];
var time: String = "";

class NotesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func addNewNote(_ sender: Any) {
        editingNote = Note();
    }
    
    var ref: FIRDatabase!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self;
        tableView.dataSource = self;
        ref = FIRDatabase.database();
        
        datePickerView.addTarget(self, action: #selector(NotesViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        
        //first init
        datePickerValueChanged(datePicker: datePickerView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:NoteCellControllerTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "noteCell") as! NoteCellControllerTableViewCell!
        
        // set the text from the data model
        cell.textLabel?.text = notes[indexPath.row].text
        cell.headerView?.text = notes[indexPath.row].created.toDay
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func showAlertDialog(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            let currentUser = FIRAuth.auth()?.currentUser;
            let time = UInt64(self.datePickerView.date.timeIntervalSince1970).toDatabaseKey;
            notes.remove(at: indexPath.row)
            
            var notesDictionaries : [[String:Any]] = [];
            for note in notes {
                var noteDict : [String: Any] = [:];
                noteDict["created"] = note.created;
                noteDict["text"] = note.text;
                notesDictionaries.append(noteDict);
            }
            self.ref.reference().child((currentUser?.uid)!).child(time).setValue(notesDictionaries);
            
            tableView.deleteRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.automatic)
        }
        delete.backgroundColor = UIColor.red
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            editingNote = notes[indexPath.row];
            self.performSegue(withIdentifier: "openNote", sender: self)
        }
        edit.backgroundColor = UIColor.lightGray
        
        let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
            let activityViewController = UIActivityViewController(activityItems: [notes[indexPath.row].text], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
        share.backgroundColor = UIColor.orange
        
        return [delete, edit, share]
    }
    
    func datePickerValueChanged(datePicker:UIDatePicker) {
        notes = [];
        tableView.reloadData();
        let currentUser = FIRAuth.auth()?.currentUser;
        time = UInt64(datePickerView.date.timeIntervalSince1970).toDatabaseKey;
        
        let node = ref.reference().child((currentUser?.uid)!).child(time);
        node.removeAllObservers();
        
        node.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let note = Note();
            note.text = value?["text"] as? String ?? "";
            note.created = value?["created"] as? UInt64 ?? 0;
            
            if notes.contains( where: { $0.created == note.created } ) == false {
                notes.append(note)
            }
            
            self.tableView.reloadData()
        }) { (error) in
            self.showAlertDialog(title: "Error", message: error.localizedDescription)
        }
    }

}
