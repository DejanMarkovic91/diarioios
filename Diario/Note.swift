//
//  Note.swift
//  Diario
//
//  Created by Dejan Markovic on 11/26/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

import Foundation

public class Note{
    var created: UnixTime = 0;
    var text: String = "";
}
