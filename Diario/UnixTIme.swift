//
//  UnixTIme.swift
//  Diario
//
//  Created by Dejan Markovic on 11/26/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

typealias UnixTime = UInt64

extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: NSDate {
        return NSDate(timeIntervalSince1970: Double(self))
    }
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull as Date)
    }
    var toDatabaseKey: String {
        return formatType(form: "d_M_yyyy_").string(from: dateFull as Date)
    }
    var toDay: String {
        return formatType(form: "dd/MM/yyyy HH:mm").string(from: dateFull as Date)
    }
}
