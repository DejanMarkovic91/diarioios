//
//  NoteEditorViewController.swift
//  Diario
//
//  Created by Dejan Markovic on 11/26/16.
//  Copyright © 2016 mozzart. All rights reserved.
//

import UIKit
import Firebase

class NoteEditorViewController: UIViewController {

    @IBOutlet weak var noteTextView: UITextView!
    
    @IBAction func save(_ sender: Any) {
        editingNote.text = noteTextView.text;
        editingNote.created = UnixTime(Int64(Date().timeIntervalSince1970 * 1000));
        let ref = FIRDatabase.database();
        let currentUser = FIRAuth.auth()?.currentUser;
        
        if notes.contains( where: { $0.created == editingNote.created } ) == false {
            notes.append(editingNote)
        }
        
        var notesDictionaries : [[String:Any]] = [];
        for note in notes {
            var noteDict : [String: Any] = [:];
            noteDict["created"] = note.created;
            noteDict["text"] = note.text;
            notesDictionaries.append(noteDict);
        }
        ref.reference().child((currentUser?.uid)!).child(time).setValue(notesDictionaries);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteTextView.text = editingNote.text;
        noteTextView.backgroundColor = UIColor.gray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "notesSaved" {
            editingNote = Note();
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
